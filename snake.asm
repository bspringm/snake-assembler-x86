.model small							;one data segement and one code segment
.stack 100h								;size of stack
.386									;add .386 directive to able to use relative jumps instructions with larger offset

.data
	snake_x DW 400 dup (48)
	snake_y DW 400 dup (48)
	snake_x_copy DW 400 dup (48)
	snake_y_copy DW 400 dup (48)
	tail_size DW 8
	fruit_size DW 8
	way DW ''
	window_is_printed DB 0
	border_x DW 0
	border_y DW 0
	last_tail_x DW 150
	last_tail_y DW 150
	grow_counter DW 0
	fruit_position DW 0
	is_fruit_there DB 0
	fruit_x DW 280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,280,16,48,240,80,24,40,16,280,280,16,48,240
	fruit_y DW 120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120,120,16,48,160,80,24,40,160,16,120
	snake_txt DW 'S','n','a','k','e','$'
	score_txt DW 'S','c','o','r','e',':','$'
	game_over_txt DW 'G','a','m','e',' ','o','v','e','r','!','$'
	highscore_txt DW 'H','i','g','h','s','c','o','r','e',':','$'
	snakelength DW 2
	score_first_place DB 48
	score_second_place DB 48
	score_place_helper DB 0
	highscore_first_place DB 1
	highscore_second_place DB 0
	scorefile DB 'sfile.txt',0
	handler DW ?
	buffer DW ?
.code
	print_snake PROC					;function to print the snake with a loop of the lentgh from snakelength			
		mov cx,last_tail_x				;initial x position
		mov dx,last_tail_y				;initial y position
		print_black_horizontal:				
										;first we overwrite the last position of the snake with a black pixel block
		mov ah,0ch						;function code to write pixel
		mov bh,00						;page number 0
		mov al,0000b   					;color black
		int 10h							;interrupt
		inc cx							;increment value in cx register
		mov ax,cx						;put cx in ax register
		sub ax,last_tail_x				;value in ax register - value at last_tail_x
		cmp ax,tail_size				;compare
		jb print_black_horizontal		;if ax < tail_size => jump to print_black_horizontal
		mov cx,last_tail_x				;put last_tail_x in cx register
		inc dx							;increment dx
		mov ax,dx						;put ax in dx register
		sub ax,last_tail_y				;value in ax register - value at last_tail_y
		cmp ax,tail_size				;compare
		jb print_black_horizontal		;if ax < tail_size => jump to print_black_horizontal
										;Now we go through all the coordinates of the snake and draw white blocks there.
		mov cx,snakelength				;cx as loop counter
	printLoop:							
		mov si,cx						;put cx in si register
		push cx							;push cx to stack
		call print_tail					;function call print_tail
		pop cx							;pop cx and decrement two times
		dec cx							;decrement value in cx register
		dec cx							;decrement value in cx register
		jne printLoop					;jump if not equal cx != 0
		ret								;return to next line at function call
	print_snake endp
	
	print_tail PROC						;function to print a tail of the snake as white block
		mov cx,offset snake_x[si]		;initial x position
		mov dx,offset snake_y[si]		;initial y position
		print_horizontal:				
		mov ah,0ch						;function code to write pixel
		mov bh,00						;page number 0
		mov al,1111b    				;color white
		int 10h							;interrupt
		inc cx							;increment value in cx register
		mov ax,cx						;put cx in ax register
		sub ax,offset snake_x[si]		;value in ax register - value at snake_x at position si
		cmp ax,tail_size				;compare
		jb print_horizontal				;if ax < tail_size => jump to print_horizontal
		mov cx,offset snake_x[si]		;put snake_x at position si in cx register
		inc dx							;increment dx
		mov ax,dx						;put dx in ax register
		sub ax,offset snake_y[si]		;value in ax register - value at snake_y at position si
		cmp ax,tail_size				;compare
		jb print_horizontal				;if ax < tail_size => jump to print_horizontal
			
		ret								;return to next line at function call
	print_tail endp
	
	window PROC							;function to create the game screen, everything is drawn once except the score
		cmp window_is_printed,1			;compare
		je score_done					;if window_is_printed = 1 => jump to score_done
		
		mov ah,00						;function code to set video mode
		mov al,13						;grafikmode 320x200 pixel ,256 colors
		int 10h							;interrupt
		
		mov ah,0bh						;function code to set background color
		mov bh,00h						;page number 0
		mov bl,00h						;color
		int 10h							;interrupt
		
		call print_border_north			;call function print_border_north
		call print_border_south			;;call function print_border_south
		
		mov si, offset snake_txt		;snake characters in si register
		mov di, 1      					;initial column position in di register
		snake_txt_loop:					
		mov ah,2h						;function code to set cursor position
		mov bh,00	  					;page number 0	
		mov dx,di   					;column number in dx register
		mov dh,0     					;row to print the text
		int 10h							;interrupt 
		lodsb         					;load current character from ds:si to al and increment si
		cmp al, '$'    					;check if string end?
		je  snake_txt_done        		;al = '$' => go to snake_txt_done | if not => al is the character to print
		mov ah,09h						;function code to print character
		mov bh,0     					;page number 0
		mov bl,11111111b     			;color(white)
		mov cx,1      					;times to print the character
		int 10h							;interrupt 
		inc di         					;increment column position
		jmp snake_txt_loop				;jump back to snake_txt_loop
	snake_txt_done:

		mov si, offset score_txt		;score characters in si register
		mov di,25      					;initial column position in di register 
		score_loop:		
		mov ah,2h						;function code to set cursor position
		mov bh,00	  					;page number 0
		mov dx,di   					;column number in dx register
		mov dh,0     					;row to print the text
		int 10h							;interrupt 
		lodsb         					;load current character from ds:si to al and increment si
		cmp al, '$'    					;check if string end?
		je  score_done       			;al = '$' => go to snake_txt_done | if not => al is the character to print
		mov ah,09h						;function code to print character
		mov bh,0     					;page number 0
		mov bl,11111111b     			;color(white)
		mov cx,1      					;times to print the character
		int 10h							;interrupt 
		inc di         					;increment column position
		jmp score_loop					;jump back to score_loop
	score_done:
	
		mov ah,2h						;function code to set cursor position
		mov dl,37						;column
		mov dh,0						;row
		mov bh,00						;page number 0
		int 10h							;interrupt
		
		mov ah,09						;function code to print character
		mov bh,00						;page number 0
		mov al,score_first_place		;character to write
		mov cx,1						;times to write
		mov bl,01111111b				;color
		int 10h 						;interrupt
		
		mov ah,2h						;function code to set cursor position
		mov dl,38						;column
		mov dh,0						;row
		mov bh,00						;page number 0
		int 10h							;interrupt
		
		mov ah,09						;function code to print character
		mov bh,00						;page number 0
		mov al,score_second_place		;character to write
		mov cx,1						;times to write
		mov bl,01111111b				;color
		int 10h 						;interrupt
		
		mov window_is_printed,1			;window_is_printed = 1
		ret								;return to next line at function call
	window endp	
	
	print_border_north PROC				;function to print the north border
		mov border_x,0					;border_x = 0
		mov border_y,7					;border_y = 7
		mov cx,320						;320 loop index in cx register
	print_border_north_horizontal:
		push cx							;push cx to the stack
		mov ah,0ch						;function code to write pixel
		mov bh,00h						;page number 0
		mov cx,border_x					;put border_x in cx register
		mov dx,border_y					;put border_y in dx register
		mov al,1111b					;color white
		int 10h							;interrupt
		
		pop cx							;pop cx from stack
		dec cx							;decrement cx
		inc border_x					;increment border_x
		cmp cx,0						;compare
		jne print_border_north_horizontal;if cx != 0 => jump to print_border_north_horizontal
		
		ret								;return to next line at function call
	print_border_north endp
	
	print_border_south PROC				;function to print the south border
		mov border_x,0					;border_x = 0
		mov border_y,192				;border_y = 192
		mov cx,320						;320 loop index in cx register
	print_border_south_horizontal:
		push cx							;push cx to the stack
		mov ah,0ch						;function code to write pixel
		mov bh,00h						;page number 0
		mov cx,border_x					;put border_x in cx register
		mov dx,border_y					;put border_y in dx register
		mov al,1111b					;color white
		int 10h							;interrupt
		
		pop cx							;pop cx from stack
		dec cx							;decrement cx
		inc border_x					;increment border_x
		cmp cx,0						;compare
		jne print_border_south_horizontal;if cx != 0 => jump to print_border_south_horizontal
		
		ret								;return to next line at function call
	print_border_south endp
	
	generate_fruit PROC					;function to generate fruit as pixel block like a tail of the snake
		cmp is_fruit_there,1			;compare 
		je is_there						;if is_fruit_there = 1 => jump to is_there
	change_position:
		inc fruit_position				;increment fruit_position
		inc fruit_position				;increment fruit_position
		mov si,fruit_position			;put fruit_position in si register		
		mov ah,0Dh						;function code to read graphic pixel
		mov cx,fruit_x[si]				;fruit_x number in cx register as column
		mov dx,fruit_y[si]				;fruit_y number in dx register as row
		mov bh,00						;page number 0
		int 10h							;interrupt
		cmp al,1111b					;compare (al = color at x/y position)
		je change_position				;if al(color) = white => jump to change_position
		
		mov is_fruit_there,1			;put 1 in is_fruit_there
	is_there:	
		mov si,fruit_position			;put fruit_position in si register	
		mov cx,fruit_x[si]				;initial x position
		mov dx,fruit_y[si]				;initial y position
		
		print_fruit_horizontal:				
		mov ah,0ch						;function code to write pixel
		mov bh,00						;page number 0
		mov al,1001b    				;color(blue)
		int 10h							;interrupt
		inc cx							;increment value in cx register
		mov ax,cx						;put cx in ax register
		sub ax,fruit_x[si]				;value in ax register - value at fruit_x at position si
		cmp ax,fruit_size				;compare
		jb print_fruit_horizontal		;if ax < fruit_size => jump to print_fruit_horizontal
		mov cx,fruit_x[si]				;put fruit_x at position si in cx register
		inc dx							;increment dx 
		mov ax,dx						;put dx in ax register
		sub ax,fruit_y[si]				;value in ax register - value at fruit_y at position si
		cmp ax,fruit_size				;compare
		jb print_fruit_horizontal		;if ax < fruit_size => jump to print_fruit_horizontal
		
		ret 							;return to next line at function call
	generate_fruit endp		
	
	check_collision PROC				;function to check a collision with the head on a tail
		mov si,snakelength				;put snakelength in si register		
		mov ah,0Dh						;function code to read graphic pixel
		mov cx,snake_x[si]				;column number in dl register (coordinate from snake_x at snakelength | head element)
		mov dx,snake_y[si]				;row number in dh register (coordinate from snake_y at snakelength | head element)
		mov bh,00						;page number 0
		int 10h							;interrupt
		cmp al,1111b					;compare (al = color at x/y position)
		je ende							;if al = white => jump to ende
		
		ret			1					;return to next line at function call
	check_collision endp
	
	calculate_score PROC				;function to calculate the score with an algorithm of the behaviour of two indepentend numbers
		cmp score_place_helper,0		;compare (score_place_helper works as 0/1 boolean)
		je second_place					;if score_place_helper = 0 => jump to second_place
		jne first_place					;if score_place_helper != 0 => jump to first_place
	first_place:
		inc score_first_place			;increment score_first_place
		cmp score_first_place,58		;compare (58 ascii for number 9)
		je convert_score				;if score_first_place = 58(9) => jump to convert_score
		jne ready						;if score_first_place != 58(9) => jump to ready
	convert_score:
		inc score_place_helper			;increment score_place_helper
		sub score_first_place,9			;score_first_place - 9 
		mov score_second_place,47		;put 47 in score_second_place
	second_place:
		cmp score_place_helper,0		;compare
		inc score_second_place			;increment score_second_place
		cmp score_second_place,58		;compare
		je convert_second_score			;if score_second_place = 58 => jump to convert_second_score
		jne ready						;if score_second_place != 58 => jump to ready
	convert_second_score:
		sub score_second_place,10		;score_second_place - 10
		inc score_first_place			;increment score_first_place
	ready:
		ret								;return to next line at function call
	calculate_score endp
	
	check_fruit PROC					;function to check the collision head/fruit
		call copy_array					;call function copy_array
		mov si,snakelength				;put snakelength in si register
		mov ah,0Dh						;function code to read graphic pixel
		mov cx,offset snake_x[si]		;column number in dl register (coordinate from snake_x at snakelength | head element)
		mov dx,offset snake_y[si]		;row number in dh register (coordinate from snake_y at snakelength | head element)
		mov bh,00						;page number 0
		int 10h							;interrupt
		cmp al,1001b					;compare (al = color at x/y position)
		je new_tail						;if al = color blue => jump to new_tail
		jne back						;if al != color blue => jump to back
	new_tail:
		mov is_fruit_there,0			;put 0 in is_fruit_there
		call calculate_score			;function call calculate_score(because of new_tail score +1)
		call copy_forward				;function call copy_forward
	back:
		ret								;return to next line at function call
	check_fruit endp
	

	
	copy_array PROC						;function to copy the arrays with the coordinates of the snake
		mov cx,snakelength				;put snakelength in cx register
	copyarrayloop:
		mov si,cx						;put cx in si register
		push cx							;push cx to stack
		mov bx,offset snake_x[si]		;put value from snake_x at si position in bx register
		mov snake_x_copy[si],bx			;now put bx in snake_x_copy at si position
		mov bx,offset snake_y[si]		;put value from snake_y at si position in bx register
		mov snake_y_copy[si],bx			;now put bx in snake_y_copy at si position
		pop cx							;pop cx from stack
		dec cx							;decrement cx
		dec cx							;decrement cx
		jne copyarrayloop				;if cx != 0 => jump to copyarrayloop
		
		ret								;return to next line at function call
	copy_array endp
	
	copy_forward PROC					;function to copy every coordinate of the snake tails forward
		inc snakelength					;increment snakelength
		inc snakelength					;increment snakelength
		mov di,snakelength				;put snakelength in di register
		dec di							;decrement di
		dec di							;decrement di
		mov cx,snakelength 				;put snakelength in cx register
	copy_forward_loop:					;copy forward every coordinate in snake_x/snake_y 
		mov si,cx						;put value from cx register in si register
		push cx							;push cx to stack
		mov dx,offset snake_y_copy[di]	;put snake_y_copy at position di in dx register	
		mov snake_y[si],dx				;now put dx in snake_y at position si
		mov dx,offset snake_x_copy[di]	;put snake_x_copy at position di in dx register	
		mov snake_x[si],dx				;now put dx in snake_x at position si
		pop cx							;pop cx from stack
		dec cx							;decrement cx
		dec cx							;decrement cx
		dec di							;decrement di
		dec di							;decrement di
		jne copy_forward_loop			;if di != 0 => jump to copy_forward_loop
		ret								;return to next line at function call
	copy_forward endp
	
	is_it_time_to_grow PROC				;function to check the grow_counter and if the grow_counter = 25 => put a new tail on the snake
		cmp grow_counter,25				;compare
		je its_time						;if grow_counter = 25 => jump to its_time
		ja its_time						;if grow_counter > 25 => jump to its_time
		jne not_yet						;if grow_counter != 25 => jump to not_yet
	its_time:
		cmp is_fruit_there,0			;compare
		je not_yet						;if is_fruit_there = 0 => jump to not_yet
		call copy_forward				;call function copy_forward
		mov grow_counter,0				;put 0 in grow_counter variable
	not_yet:
		inc grow_counter				;increment grow_counter
	
	ret									;return to next line at function call
	is_it_time_to_grow endp
	
	shift_back PROC						;function to shift back the coordinates to previous one coordinate element
		mov cx,snake_x[2]				;put value at index 2 in cx register		
		mov dx,snake_y[2]				;put value at index 2 in dx register
		mov last_tail_x,cx				;put cx in last_tail_x
		mov last_tail_y,dx				;put cx in last_tail_y
	
		call copy_array					;call function copy_array
		cmp snakelength,2				;compare
		je return						;if snakelength = 2 => jump to return
	
		mov cx,snakelength				;put snakelength in cx register
		dec cx							;decrement cx
		dec cx							;decrement cx
		mov di,snakelength				;put snakelength in di register
	shiftloop: 
		mov si,cx						;put cx in si register
		push cx							;push cx to stack
		mov bx,snake_y_copy[di]			;put value from snake_y_copy at di position in bx register
		mov snake_y[si],bx				;now put bx in snake_y at si position
		mov bx,snake_x_copy[di]			;put value from snake_x_copy at di position in bx register
		mov snake_x[si],bx				;now put bx in snake_x at si position
		dec di							;decrement di
		dec di							;decrement di
		pop cx							;pop cx from stack
		dec cx							;decrement cx
		dec cx							;decrement cx
		jne shiftloop					;if cx != 0 => jump to shiftloop
	return:
		ret								;return to next line at function call
	shift_back endp
	
	game_over_loop PROC					;function at the end of the game to show score/highscore and wait for escape
	end_looping:
		mov ah,0ch						;function code to flush keyboard
		mov al,0					
		int 21h							;interrupt
		
		mov ah,0bh						;function code to set background color
		mov bh,00						;page number 0
		mov bl,11h						;background color (black)
		int 10h							;interrupt
		
		mov si, offset game_over_txt	;Game over characters in si register
		mov di, 11      				;initial column position in di register
		game_over_txt_loop:						
		mov ah,2h						;function code to set cursor position
		mov bh,00	  					;page number 0
		mov dx,di   					;column number in dx register
		mov dh,10     					;row to print the text
		int 10h							;interrupt
		lodsb         					;load current character from ds:si to al and increment si
		cmp al, '$'    					;check if string end?
		je  game_over_txt_done     		;al = '$' => go to game_over_txt_done | if not => al is the character to print
		mov ah,09h						;function code to print character
		mov bh,0     					;page number 0
		mov bl,11111111b  				;color(white)
		mov cx,1						;times to print the character
		int 10h							;interrupt
		inc di         					;increment column position
		jmp game_over_txt_loop			;jump back to game_over_txt_loop
		
	game_over_txt_done:
		mov si, offset score_txt		;score characters in si register
		mov di, 11     					;initial column position in di register
		score_txt_loop:		
		mov ah,2h						;function code to set cursor position
		mov bh,00						;page number 0
		mov dx,di   					;column number in dx register
		mov dh,12     					;row to print the text
		int 10h							;interrupt 
		lodsb         					;load current character from ds:si to al and increment si
		cmp al, '$'    					;check if string end?
		je  score_txt_done        		;al = '$' => go to game_over_txt_done | if not => al is the character to print
		mov ah,09h						;function code to print character
		mov bh,0     					;page number 0
		mov bl,11111111b     			;color(white)
		mov cx,1      					;times to print the character
		int 10h							;interrupt 
		inc di   						;increment column position
		jmp score_txt_loop				;jump back to score_txt_loop
	
	score_txt_done:
		mov si, offset highscore_txt	;highscore characters in si register
		mov di, 11      				;initial column position in di register
		highscore_txt_loop:				
		mov ah,2h						;function code to set cursor position
		mov bh,00	  					;page number 0
		mov dx,di   					;column number in dx register
		mov dh,14     					;row to print the text
		int 10h							;interrupt 
		lodsb         					;load current character from ds:si to al and increment si
		cmp al, '$'    					;check if string end?
		je  highscore_txt_done     		;al = '$' => go to game_over_txt_done | if not => al is the character to print
		mov ah,09h						;function code to print character
		mov bh,0     					;page number 0
		mov bl,11111111b     			;color(white)
		mov cx,1      					;times to print the character
		int 10h							;interrupt 
		inc di         					;increment column position
		jmp highscore_txt_loop			;jump back to highscore_txt_loop
		
	highscore_txt_done:
		mov ah,2h						;function code to set cursor position
		mov dl,25						;column number in dl register
		mov dh,12						;row number in dh register
		mov bh,00						;page number 0
		int 10h							;interrupt 
		
		mov ah,09						;function code to print character
		mov bh,00						;page number 0
		mov al,score_first_place		;ascii for character from score_first_place in al register
		mov cx,1						;times to print the character
		mov bl,11111111b				;color(white)
		int 10h 						;interrupt 
		
		mov ah,2h						;function code to set cursor position
		mov dl,26						;column number in dl register
		mov dh,12						;row number in dh register
		mov bh,00						;page number 0
		int 10h							;interrupt
		
		mov ah,09						;function code to print character
		mov bh,00						;page number 0
		mov al,score_second_place		;ascii for character from score_first_place in al register
		mov cx,1						;times to print the character
		mov bl,11111111b				;color(white)
		int 10h 						;interrupt
		
		mov ah,3dh						;function code to open file
		mov al,2						;permission to read/write
		lea dx,scorefile				;address of filename in dx register
		int 21h							;interrupt
		mov handler,ax					;file handle

		mov ah,3fh						;function code to read from file
		mov cx,3						;length of characters to read
		mov bx,handler					;file handle
		lea dx,highscore_first_place	;address from highscore_first_place in dx and so highscore_first_place is the buffer for data
		int 21h							;interrupt
		
		mov ah,42h						;function code to set new file position
		mov al,0						;calculate new position, 0 = start of file + dx:cx
		mov cx,2						;offset from origin of new file position (position 2)
		mov dx,0						;offset from origin of new file position
		mov bx,handler					;file handle
		int 21h							;interrupt
		
		mov ah,3fh						;function code to read from file
		mov cx,1						;length of characters to read
		mov bx,handler					;file handle
		lea dx,highscore_second_place	;address from highscore_second_place in dx and so highscore_second_place is the buffer for data
		int 21h							;interrupt
		
		mov ah,2h						;function code to set cursor position
		mov dl,31						;column number in dl register
		mov dh,14						;row number in dh register
		mov bh,00						;page number 0
		int 10h							;interrupt
		
		mov ah,09						;function code to print character
		mov bh,00						;page number 0
		mov al,highscore_first_place	;ascii for character from highscore_first_place in al register
		mov cx,1						;times to print the character
		mov bl,11111111b				;color(white)
		int 10h 						;interrupt
		
		mov ah,2h						;function code to set cursor position
		mov dl,32						;column number in dl register
		mov dh,14						;row number in dh register
		mov bh,00						;page number 0
		int 10h							;interrupt	
		
		mov ah,09						;function code to print character
		mov bh,00						;page number 0
		mov al,highscore_second_place	;ascii for character from highscore_second_place in al register
		mov cx,1						;times to print the character
		mov bl,11111111b				;color(white)
		int 10h 						;interrupt	
		
										;here comes the verification whether the programm need to set a new highscore
		cmp highscore_first_place,48	;compare
		je test_highscore_second_place	;if highscore_first_place = 48 => jump to test_highscore_second_place
		jne compare_first_numbers		;if highscore_first_place != 48 => jump to compare_first_numbers
	test_highscore_second_place:
		cmp highscore_second_place,48	;compare
		je write_in_file				;if highscore_second_place = 48 => jump to write in file
		jne compare_first_numbers		;if highscore_second_place = 48 => jump to compare_first_numbers
	compare_first_numbers:
		mov dl,highscore_first_place	;put highscore_first_place in dl register
		cmp score_first_place,dl		;compare
		ja write_in_file				;if score_first_place > highscore_first_place => jump to write_in_file
		jb wait_for_esc					;if score_first_place < highscore_first_place => jump to wait_for_esc
		je compare_second_numbers
	compare_second_numbers:
		mov dl,highscore_second_place	;put highscore_second_place in dl register
		cmp score_second_place,dl		;compare
		ja write_in_file				;if score_second_place > highscore_second_place => jump to write_in_file
		jb wait_for_esc					;if score_second_place < highscore_second_place => jump to wait_for_esc
		je wait_for_esc					;if score_second_place = highscore_second_place => jump to wait_for_esc
		
	write_in_file:
		mov ah,42h						;function code to set new file position
		mov al,0						;calculate new position, 0 = start of file + dx:cx
		mov cx,0						;offset from origin of new file position
		mov dx,0 						;offset from origin of new file position
		mov bx,handler					;file handle
		int 21h							;interrupt
		
		mov  ah, 40h					;function code to write in file
		mov  bx, handler				;file handle
		mov  cx, 2  					;length of characters to write
		lea  dx,score_first_place		;address from score_first_place in dx and so score_first_place is the buffer for data to write
		int  21h						;interrupt
		
		mov  ah, 3eh					;function code to close and save file
		mov  bx, handler				;file handle
		int  21h						;interrupt
		
	wait_for_esc:						;loop to wait for user input(esc)
		int 15h							;interrupt BIOS wait function
		mov ah,0ch						;function code to flush keyboard
		mov al,0						
		int 21h							;interrupt
	
		mov ax,0
		int 16h							;interrupt
		
		cmp al, 1bh						;compare (in al user input from keyboard) 1bh = esc
		jne wait_for_esc				; if al != esc => jump to wait_for_esc
		
		ret								;return to next line at function call
	game_over_loop endp

	start:								;start of the programm
		mov ax,@data					;load ds data register
		mov ds,ax						;put ax in ds register
		
		call window						;call function window
		call print_snake				;call function print_snake
		call generate_fruit				;call function generate_fruit
	play:
		mov cx,0						;put 0 in cx register
		push cx							;push cx to stack
	mainloop:	
		mov ah,0ch						;function code to flush keyboard
		mov al,0						
		int 21h							;interrupt
		
		pop cx							;pop cx from stack
		cmp cx,1						;compare
		je move_up						;if cx = 1 => jump to move_up
		cmp cx,2						;compare
		je move_down					;if cx = 2 => jump to move_down
		cmp cx,3						;compare
		je move_left					;if cx = 3 => jump to move_left
		cmp cx,4						;compare
		je move_right					;if cx = 3 => jump to move_right
		
		call window						;call function window
		call print_snake				;call function print_snake
		call generate_fruit				;call function generate_fruit
		
										;know we test the border / head collision
		mov si,snakelength
		cmp snake_x[si],-1				;compare
		je ende							;if coordinate at snake_x[si] = -1 => jump to ende
		cmp snake_x[si],312				;compare
		ja ende							;if coordinate at snake_x[si] > 312 => jump to ende
		cmp snake_y[si],8				;compare
		jb ende							;if coordinate at snake_y[si] < 8 => jump to ende
		cmp snake_y[si],190				;compare
		ja ende							;if coordinate at snake_y[si] > 190 => jump to ende
										;know test the total game score
		cmp score_first_place,57		;compare
		jne nothing_more_to_check		;if score_first_place = 57 => go next line
		cmp score_second_place,57		;compare
		je ende							;if score_second_place = 57 => jump to ende | then the gamer got the total score
	
	nothing_more_to_check:	
		mov ah,86h						;function code to wait
		mov cx,2						;cx:dx intervall in seconds
		mov dx,0h
		int 15h							;interrupt
		
		mov ah,01h						;function code to check for keystroke in the keyboard buffer
		int 16h							;interrupt
		je mainloop						;if ZF = 1 if keystroke is not available => jump to mainloop
		
		cmp al,77h						;compare 77h = W
		je move_up						;if al = 77h => jump to move_up
		cmp al,73h						;compare 73h = S
		je move_down					;if al = 73h => jump to move_down
		cmp al,61h						;compare 61h = A
		je move_left					;if al = 61h => jump to move_left
		cmp al,64h						;compare 64h = D
		je move_right					;if al = 64h => jump to move_right
		cmp al, 1bh						;compare 1bh = esc
		jne play						; if al != esc => jump to play
		jmp ende						;jump to ende
		
	move_up:							
		cmp way,'d'						;compare
		je mainloop						;if way = 'd' => jump to mainloop
		call shift_back					;call function shift_back
		mov si,snakelength				;put snakelength in si register
		sub snake_y[si],8				;snake_y at position si - 8 (snakehead)
		call check_collision			;call function check_collision
		call check_fruit				;call function check_fruit
		call is_it_time_to_grow			;call function is_it_time_to_grow
		mov cx, 1						;put 1 in cx register
		push cx							;push cx register to stack
		mov way,'u'						;put 'u' in way value
		jmp play						;jump to play
		
	move_down:
		cmp way,'u'						;compare
		je mainloop						;if way = 'u' => jump to mainloop
		call shift_back					;call function shift_back
		mov si,snakelength				;put snakelength in si register
		add snake_y[si],8				;snake_y at position si + 8 (snakehead)
		call check_collision			;call function check_collision
		call check_fruit				;call function check_fruit
		call is_it_time_to_grow			;call function is_it_time_to_grow
		mov cx, 2						;put 2 in cx register
		push cx							;push cx register to stack
		mov way,'d'						;put 'd' in way value
		jmp play						;jump to play
	
	move_left:
		cmp way,'r'						;compare
		je mainloop						;if way = 'r' => jump to mainloop
		call shift_back					;call function shift_back
		mov si,snakelength				;put snakelength in si register
		sub snake_x[si],8				;snake_x at position si - 8 (snakehead)
		call check_collision			;call function check_collision
		call check_fruit				;call function check_fruit
		call is_it_time_to_grow			;call function is_it_time_to_grow
		mov cx, 3						;put 3 in cx register
		push cx							;push cx register to stack
		mov way,'l'						;put 'l' in way value
		jmp play						;jump to play
		
	move_right:
		cmp way,'l'						;compare
		je mainloop						;if way = 'l' => jump to mainloop
		call shift_back					;call function shift_back
		mov si,snakelength				;put snakelength in si register
		add snake_x[si],8				;snake_x at position si + 8 (snakehead)
		call check_collision			;call function check_collision
		call check_fruit				;call function check_fruit
		call is_it_time_to_grow			;call function is_it_time_to_grow
		mov cx, 4  						;put 4 in cx register
		push cx							;push cx register to stack
		mov way,'r'						;put 'r' in way value
		jmp play						;jump to play
		
	ende:
		mov ah,0ch						;function code to flush keyboard
		mov al,0						
		int 21h							;interrupt
		
		mov window_is_printed,0			;window_is_printed = 0
		call window						;call function window
		call game_over_loop				;call function game_over_loop
		mov ax,3						;delete the game window
		int 10h							;interrupt
		
		mov ah,00h						;function code for video mode
		mov al,3h						;screen mode
		int 10h							;interrupt				
		
		mov ah,4ch						;function code to return to DOS
		int 21h							;interrupt
	end start